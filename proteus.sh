#!/bin/bash
#https://bit.ly/2owivia
apt -y update;
apt -y upgrade;

#Essential software
apt -y install sakura;
apt -y install pcmanfm;
apt -y install wget;
apt -y install tint2;
apt -y install git;

#Extra Software
apt -y install nitrogen;
apt -y install lxtask;
apt -y install screenfetch;
apt -y install leafpad;
apt -y install gmusicbrowser;
apt -y install deluge;
apt -y install audacity;
apt -y install mpv;
apt -y install krita;
apt -y install mkvtoolnix-gui;
apt -y install lxappearance;
apt -y install pavucontrol;
apt -y install putty;
apt -y install xtightvncviewer;
apt -y install compton;
apt -y install conky;
apt -y install krita;
apt -y install unzip;

#Configurations
#Create initial folders
mkdir /home/proteus/.config;
chown proteus:proteus /home/proteus/.config;
mkdir /home/proteus/.config/openbox;
chown proteus:proteus /home/proteus/.config/openbox;
mkdir /home/proteus/Downloads;
chown proteus:proteus /home/proteus/Downloads;
mkdir /home/proteus/.config/compton;
chown proteus:proteus /home/proteus/.config/compton;
mkdir /home/proteus/.config/tint2;
chown proteus:proteus /home/proteus/.config/tint2;

#Install Google Chrome Stable
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -;
sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list';

#Install resilio sync
wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | apt-key add -;
echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | tee /etc/apt/sources.list.d/resilio-sync.list;

#Set screenfetch on boot
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch; fi" >> /home/proteus/.bashrc;

#Installing compton config file
cd /home/proteus/Downloads;
wget https://gitlab.com/dwt1/dotfiles/raw/master/.config/compton/compton.conf;
chown proteus:proteus compton.conf;
mv compton.conf /home/proteus/.config/compton/;

#Installing openbox menu file
cd /home/proteus/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/proteus-menu.xml;
mv proteus-menu.xml menu.xml;
chown proteus:proteus menu.xml;
mv menu.xml /home/proteus/.config/openbox/;

#Insatlling openbox keybinds
cd /home/proteus/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/rc.xml;
chown proteus:proteus rc.xml;
mv rc.xml /home/proteus/.config/openbox/;

#Installing tint2 config file
cd /home/proteus/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/tint2rc;
chown proteus:proteus tint2rc;
mv tint2rc /home/proteus/.config/tint2/;

#Installing conky config file
cd /home/proteus/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/lite.conkyrc;
chown proteus:proteus lite.conkyrc;
mv lite.conkyrc /home/proteus/.conkyrc;

#Installing Discord
cd /home/proteus/Downloads;
su - proteus -c 'wget -O /home/proteus/Downloads/discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"';
dpkg -i discord.deb;
apt -f -y install;
rm discord.deb;

#Set up autostart.sh
touch /home/proteus/.config/openbox/autostart.sh;
chown proteus:proteus /home/proteus/.config/openbox/autostart.sh;
echo "tint2 &" >> /home/proteus/.config/openbox/autostart.sh;
echo "nitrogen --restore &" >> /home/proteus/.config/openbox/autostart.sh;
echo "compton --config /home/proteus/.config/compton/compton.conf &" >> /home/proteus/.config/openbox/autostart.sh;
echo "conky &" >> /home/proteus/.config/openbox/autostart.sh;
echo "rslsync &" >> /home/proteus/.config/openbox/autostart.sh;


#Installing new repo software
apt -y update;
apt -y install google-chrome-stable;
apt -y install resilio-sync;
sync;
echo "Please reboot now"