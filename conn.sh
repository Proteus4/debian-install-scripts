#!/bin/bash
#https://bit.ly/2ov169G
apt -y update;
apt -y upgrade;

#Essential software
apt -y install sakura;
apt -y install pcmanfm;
apt -y install tint2;
apt -y install curl;
apt -y install apt-transport-https;
apt -y install openssh-server;

#Extra Software
apt -y install gedit;
apt -y install nitrogen;
apt -y install screenfetch;
apt -y install lxappearance;
apt -y install conky;
apt -y install tightvncserver;
apt -y install deluged;
apt -y install deluge-console;
apt -y install samba;
apt -y install gnome-disk-utility;
apt -y install htop;
apt -y install default-jdk;
apt -y install unzip;
apt -y install chromium;
apt -y install screen;

#Configurations
#Create initial files / folders
mkdir /home/conn/.config;
chown conn:conn /home/conn/.config;
mkdir /home/conn/.config/openbox;
chown conn:conn /home/conn/.config/openbox;
mkdir /home/conn/Downloads;
chown conn:conn /home/conn/Downloads;
mkdir /home/conn/.config/tint2;
chown conn:conn /home/conn/.config/tint2;
mkdir /home/conn/deluge
chown conn:conn /home/conn/deluge
mkdir /etc/samba;
touch /home/conn/.config/openbox/autostart.sh;
chown conn:conn /home/conn/.config/openbox/autostart.sh;
mkdir /home/conn/.themes;
chown conn:conn /hone/conn/.themes;

#Install resilio sync
wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | apt-key add -;
echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | tee /etc/apt/sources.list.d/resilio-sync.list;

#Installing plex media server
curl https://downloads.plex.tv/plex-keys/PlexSign.key | apt-key add -;
echo deb https://downloads.plex.tv/repo/deb public main | tee /etc/apt/sources.list.d/plexmediaserver.list;

#Installing MKVToolNix
wget -q -O - https://mkvtoolnix.download/gpg-pub-moritzbunkus.txt | sudo apt-key add -;
echo "deb https://mkvtoolnix.download/debian/ buster main" > /etc/apt/sources.list.d/mkvtoolnix.download.list;

#Set screenfetch on boot
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch; fi" >> /home/conn/.bashrc;

#Installing openbox menu file
cd /home/conn/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/conn-menu.xml;
mv conn-menu.xml menu.xml;
chown conn:conn menu.xml;
mv menu.xml /home/conn/.config/openbox/;

#Installing tint2 config file
cd /home/conn/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/tint2rc;
chown conn:conn tint2rc;
mv tint2rc /home/conn/.config/tint2/;

#Installing conky config file
cd /home/conn/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/lite.conkyrc;
chown conn:conn lite.conkyrc;
mv lite.conkyrc /home/conn/.conkyrc;

#Installing samba
cd /home/conn/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/smb.conf;
mv smb.conf /etc/samba/;

#Installing deluged
cd /home/conn/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/core.conf;
chown conn:conn core.conf;
mv core.conf /home/conn/deluge/;

#Installing Windows 10 Dark Theme
wget https://gitlab.com/Proteus4/debian-install-scripts/raw/master/.themes.zip;
chown conn:conn .themes.zip;
mv .themes.zip /home/conn/.themes/;
unzip /home/conn/.themes/.themes.zip;
rm /home/conn/.themes/.themes.zip;

#Set up autostart.sh
echo "tint2 &" >> /home/conn/.config/openbox/autostart.sh;
echo "nitrogen --restore &" >> /home/conn/.config/openbox/autostart.sh;
echo "conky &" >> /home/conn/.config/openbox/autostart.sh;
echo "rslsync &" >> /home/conn/.config/openbox/autostart.sh;
echo "deluged &" >> /home/conn/.config/openbox/autostart.sh;

#Installing new repo software
apt -y update;
apt -y install resilio-sync;
apt -y install plexmediaserver;
apt -y install mkvtoolnix-gui;
sync;

echo "Please manually download and install JDownloader";
echo "Link: http://mega.nz/#!LJ9FyK7b!t88t6YBo2Wm_ABkSO7GikxujDF5Hddng9bgDb8fwoJQ";
echo "Please create auth file in /home/conn/deluge/auth";
echo "Syntax: user:password:10";